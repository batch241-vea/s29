
// COMPARISON QUERY OPERATORS

// $gtg / $gte operator
/*
	- allows us to have documents that have field number values greater than or equal to specified value.
	- Syntax:
		db.collectionName.find({field: {$gt: value}})
		db.collectionName.find({field: {$gte: value}})
*/

db.users.find({ age: {$gt: 65}});

db.users.find({ age: {$gte: 65}});

// $lt / $lte operator
/*
	-	allows us to have documents that have field number values less than or equal to specified value.
	- Syntax:
		db.collectionName.find({field: {$lp: value}});
		db.collectionName.find({field: {$lpe: value}});
*/

db.users.find({age: {$lt: 76}});
db.users.find({age: {$lte: 76}});


// $ne operator
/*
	-	allows us to have documents that have field number values that are not equal to specified value.
	- Syntax:
		db.collectionName({field: {$ne: value}});
*/

db.users.find({age: {$ne: 82}});



// LOGICAL QUERY OPERATORS

// $or operator
/*
	-	Allows us to find a document that match a single criteria from multiple search criteria provided.
	- Syntax:
		db.collectionName.find({$or: [{fieldA: "valueA"}, {fieldB:"valueB"}]});
*/

db.users.find({$or: [{firstName: "Neil"},{firstName: "Bill"}]});
db.users.find({$or: [{firstName: "Neil"},{firstName: "Bill"}, {firstName: "Stephen"}, {firstName: "Jane"}]});

db.users.find({$or: [{firstName: "Neil"}, {age: {$gt: 30}}]});


// $and operator
/*
	-	allows us to find documents matching multiple criteria in a single field.
	- Syntax:
		db.collectionName({$and: [{fieldA:valueA}, {fieldB:valueB}]
		});
*/
db.users.find({$and: [{age: {$ne: 82}}, {age:{$ne: 76}}] });

// db.users.find({$and: [{age:{$lt:65}, {age: {$gt:65}}}]});



//FIELD PEOJECTION
/*
	- when we retrieve documents, MongoDB usually returns the whole document.
	- there are times when we only need specific fields
	- for those cases, we can include or exclude fields from the response
*/

//INCLUSION
/*
	- allows us to include / add specific fields only when retrieving documents,
	- we write 1 to include fields
	- Syntax:
		db.users.find({criteria}, {field:1});
*/
db.users.find(
{
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	"contact.phone": 1
}
)


// EXCLUSION
/*
	- Allows us to exclude / remove specific fields when displaying documents
	- The value provided is 0 to denote that the field is being excluded.
	- Syntax:
		db.users.find({criteria}, {fiend:0});
*/
db.users.find(
	{
		firstName: "Jane"
	}, 
	{
		contact:0, 
		department: 0
	}
);

//Suppressing the ID field
/*
	- allows us to exclude the "_id" field when retrieving the documents
	- when using field projection, field inclusion and exclusion may not be used at the same time.
	- The "_id" is exempted to this rule.
	- Syntax:
		db.users.find({criteria}, {_id:0});
*/
db.users.find(
{
	firstName:"Jane"
},
{
	firstName: 1,
	lastName: 1,
	contact: 1,
	_id: 0
}
);


// EVALUATION OF QUERY OPERATORS

// $regex operator
/*
	- allows us to find documents that match a specific string pattern using regular expressions.
	- syntax:
		db.users.find(
		{
			field: {$regex: 'pattern', $options: '$optionsValue'}
		})
*/

//case sensitive query
db.users.find(
{
	firstName: {$regex: 'N'}
})

//case insensitive query
db.users.find(
{
	firstName: {$regex: 'N', $options: '$i'}
})
